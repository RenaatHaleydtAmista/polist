sap.ui.controller("be.amista.purchaseorderlist.ext.controller.ListReportExt", {

    onInitSmartFilterBarExtension: function (oEvent) {

        //Get SmartFilterBar 
        var oGlobalFilter = oEvent.getSource();

        // January 1st of previous year until today
        var oToday = new Date();
        var oKeyDate = new Date(new Date().getFullYear() - 1, 0, 1);

        var oDefaultFilter = {
            "CreationDate": {
                "ranges": [{
                    "exclude": false,
                    "operation": "GE",
                    "keyField": "CreationDate",
                    "value1": oKeyDate,
                    "value2": oToday
                }]
            }
        };

        //Set SmartFilterBar default values
        oGlobalFilter.setFilterData(oDefaultFilter);
    }
});